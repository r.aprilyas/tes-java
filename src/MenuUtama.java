import java.util.Scanner;

public class MenuUtama extends SoalAbstact implements IMenuUtama{

    @Override
    public void menu() {
        try {
            System.out.print("Masukan Pilihan\n1.Jawaban Nomor 1\n2.Jawaban Nomor 2\n3.Jawaban Nomor 3\n4.Exit\npil\t: ");
            int pil = Integer.parseInt(in.nextLine());
            if(pil == 1){
                JawabanNomor1 obNomor1 =new JawabanNomor1();
                obNomor1.deklarasi();
            }
            else if(pil == 2){
                JawabanNomor2 obNomor2 =new JawabanNomor2();
                obNomor2.deklarasi();
            }
            else if(pil == 3){
                JawabanNomor3 obNomor3 =new JawabanNomor3();
                obNomor3.deklarasi();
            }
            else if(pil == 4){
                System.exit(0);
            }
            else{
                System.out.print("Tidak Ada Pilihan Masukan Lagi\n");
                menu();
            }
        }catch (NumberFormatException e){
            System.out.println("masukan angka bukan huruf");
            menu();
        }catch (NullPointerException e){
            System.out.println("tidak boleh null");
            menu();
        }
    }

    public static void main(String[] args){
        MenuUtama obMenu =new  MenuUtama();
        obMenu.menu();
    }
}
