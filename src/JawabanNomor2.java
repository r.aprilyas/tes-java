import java.util.ArrayList;
import java.util.Scanner;

//jawaban nomor 2
public class JawabanNomor2 extends SoalAbstact implements AllFunction, IJawabanNomor2 {
    private int[] nums;
    private ArrayList<Integer> tidakX = new ArrayList<>();

    private int x = 0;

    /**
     * menghandle enter nextLine
     */
    @Override
    public void handleNextLine() {
        if (firstTime == true) {
            System.out.print("tekan enter untuk memulai");
            in.nextLine();
            firstTime = false;
        }
    }

    @Override
    public void deklarasi() {
        handleNextLine();

        try {
            System.out.print("masukan banyak array : ");
            int banyakArray = Integer.parseInt(in.nextLine());

            //menentukan banyak array pada index
            nums = new int[banyakArray];
            System.out.print("\n");
            inputIndex();
        } catch (NumberFormatException e) {
            System.out.println("masukan angka bukan huruf");
            deklarasi();
        } catch (NullPointerException e) {
            System.out.println("tidak boleh null");
            deklarasi();
        }
    }

    /**
     * input index array
     */
    @Override
    public void inputIndex() {
        handleNextLine();

        try {
            for (int i = 0; i < nums.length; i++) {
                System.out.print("masukan index ke " + i + " : ");
                nums[i] = Integer.parseInt(in.nextLine());
            }
            System.out.print("\n");
            tidakSamaNilaiX();
        } catch (NumberFormatException e) {
            System.out.println("masukan angka bukan huruf");
            inputIndex();
        } catch (NullPointerException e) {
            System.out.println("tidak boleh null");
            inputIndex();
        }
    }

    /**
     * untuk tidak mengembalikan nilai x pada nilai array
     */
    @Override
    public void tidakSamaNilaiX() {
        handleNextLine();

        System.out.print("masukan nilai x : ");
        x = Integer.parseInt(in.nextLine());

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != x) {
                tidakX.add(nums[i]);
            }
        }

        System.out.println("\noutput : "+tidakX + "\n");
        bagi();
    }

    /**
     * untuk membuktikan atau menandai nilai x dengan cara pembagian
     */
    @Override
    public void bagi() {
        float hasil;

        for (int i = 0; i < nums.length; i++) {
            hasil = ((float) nums[i] / x);
            if (hasil != 1) {
                System.out.println("hasil dari " + (float) nums[i] + " / " + x + " = " + String.format("%2.02f", hasil));
            } else {
                System.out.println("hasil dari " + nums[i] + " / " + x + " = " + String.format("%2.0f", hasil) + " (tandai nilai ini)");
            }
        }

        System.out.println("\n");
        MenuUtama menuUtama = new MenuUtama();
        menuUtama.menu();
    }
}
