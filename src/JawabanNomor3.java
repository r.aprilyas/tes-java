import java.util.ArrayList;
import java.util.Scanner;

//jawaban nomor 3
public class JawabanNomor3 extends SoalAbstact implements AllFunction, IJawabanNomor3 {
    private ArrayList<String[]> words = new ArrayList<>();

    private String word = "";

    private int x = 0;

    private Boolean notFount = true;

    /**
     * menghandle enter nextLine
     */
    @Override
    public void handleNextLine() {
        if (firstTime == true) {
            System.out.print("tekan enter untuk memulai");
            in.nextLine();
            firstTime = false;
        }
    }

    @Override
    public void deklarasi() {
        handleNextLine();

        try {
            System.out.print("masukan kalimat\t\t\t:");
            word = in.nextLine();

            inputIndex();
        } catch (NumberFormatException e) {
            System.out.println("masukan angka bukan huruf");
            deklarasi();
        } catch (NullPointerException e) {
            System.out.println("tidak boleh null");
            deklarasi();
        }
    }

    /**
     * input index kata
     */
    @Override
    public void inputIndex() {
        handleNextLine();

        try {
            System.out.print("masukan panjang kata\t:");
            x = Integer.parseInt(in.nextLine());

            //untuk memecah kalimat menjadi kata
            words.add(word.split(" "));

            System.out.print("\n");
            pemisahanKata();
        } catch (NumberFormatException e) {
            System.out.println("masukan angka bukan huruf");
            inputIndex();
        } catch (NullPointerException e) {
            System.out.println("tidak boleh null");
            inputIndex();
        }
    }

    /**
     * untuk mengecek manakah panjang kata
     * yang sesui dengan panjang yang diinginkan
     */
    @Override
    public void pemisahanKata() {
        System.out.print("output : ");
        for (String[] i : words) {
            for (String k : i) {
                if (k.length() == x) {
                    System.out.print(k+" ");
                    notFount = false;
                }
                if (notFount) {
                    notFount = true;
                }
            }
        }

        if (notFount) {
            System.out.print("tidak ditemukan banyak kata dari : "+x);
        }

        System.out.print("\n\n");
        cekPanjang();
    }

    /**
     * untuk melhat panjang setiap kata yang ada
     * dan menandai jika sesuai
     */
    @Override
    public void cekPanjang() {
        for (String[] i : words) {
            for (String k : i) {
                if (k.length() == x) {
                    System.out.println("'"+k+"'"+" panjang katanya : "+k.length()+" (sesuai)");
                }
                else{
                    System.out.println("'"+k+"'"+" panjang katanya : "+k.length());
                }
            }
        }

        System.out.println("\n");
        MenuUtama menuUtama = new MenuUtama();
        menuUtama.menu();
    }
}
