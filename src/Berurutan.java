import java.util.ArrayList;
import java.util.Scanner;

public class Berurutan {
    static Scanner in = new Scanner(System.in);
    static int nilai [];

    static ArrayList<Integer> urutkan = new ArrayList<>();
    static ArrayList<Integer> urutkan2 = new ArrayList<>();
    static ArrayList<Integer> urutkanTampung = new ArrayList<>();
    static int tampung;
    static int jumlah;
    static boolean statusOut = false;

    static void proses(){
        System.out.print("masukan panjang array : ");
        int panjang = in.nextInt();
        nilai =new int [panjang];

        //proses input nilai
        for(int i=0; i < nilai.length; i++){
            System.out.print("masukan nilai : ");
            nilai[i]= in.nextInt();
        }

        //proses sorting
        for(int i=0; i < nilai.length; i++){
            for(int k=i+1; k < nilai.length; k++){
                if(nilai[i] >= nilai[k]){
                    tampung = nilai[k];
                    nilai[k] = nilai[i];
                    nilai[i] = tampung;
                }
            }
        }

        //cetak
        for(int i=0; i < nilai.length; i++){
            System.out.print(nilai[i]+" ");
        }

        System.out.print("\n");

        //proses penentuan panjang berurut
        for(int i=0; i < nilai.length-1; i++){
            jumlah = nilai[i]+1;

            if(jumlah == nilai[i+1]){
                if(statusOut == false){
                    if (urutkan.size() == 0) {
                        urutkan.add(jumlah - 1);
                    }

                    urutkan.add(jumlah);
                }
                else{
                    if (urutkan2.size() == 0) {
                        urutkan2.add(jumlah - 1);
                    }

                    urutkan2.add(jumlah);
                }
            }
            else{
                if(urutkan2.size() < urutkan.size()){
                    urutkan2.clear();
                    urutkanTampung = urutkan;

                    statusOut = true;
                }
                else{
                    urutkan.clear();
                    urutkanTampung = urutkan2;

                    statusOut = false;
                }
            }
        }

        System.out.print(urutkanTampung);
    }
    public static void main(String[] args) {
        proses();
    }
}
