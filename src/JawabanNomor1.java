import java.util.ArrayList;
import java.util.Scanner;


//jawaban nomor 1
public class JawabanNomor1 extends SoalAbstact implements AllFunction, IJawabanNomor1 {

    private ArrayList<Integer> nums = new ArrayList<>();

    private int nilaiTerbesar = 0;
    private int banyakArray = 0;

    /**
     * menghandle enter nextLine
     */
    @Override
    public void handleNextLine() {
        if (firstTime == true) {
            System.out.print("tekan enter untuk memulai");
            in.nextLine();
            firstTime = false;
        }
    }

    @Override
    public void deklarasi() {
        handleNextLine();

        try {
            System.out.print("masukan banyak array : ");
            banyakArray = Integer.parseInt(in.nextLine());
            System.out.print("\n");
            inputIndex();
        } catch (NumberFormatException e) {
            System.out.println("masukan angka bukan huruf");
            deklarasi();
        } catch (NullPointerException e) {
            System.out.println("tidak boleh null");
            deklarasi();
        }
    }

    /**
     * input index array
     */
    @Override
    public void inputIndex() {
        handleNextLine();

        try {
            for (int i = 0; i < banyakArray; i++) {
                System.out.print("masukan index ke " + i + " : ");
                nums.add(Integer.parseInt(in.nextLine()));
            }
            System.out.print("\n");
            cekNilaiTerbesar();
        } catch (NumberFormatException e) {
            System.out.println("masukan angka bukan huruf");
            inputIndex();
        } catch (NullPointerException e) {
            System.out.println("tidak boleh null");
            inputIndex();
        }
    }

    /**
     * mencari nilai terbesar
     */
    @Override
    public void cekNilaiTerbesar() {
        for (int num : nums) {
            if (nilaiTerbesar < num) {
                nilaiTerbesar = num;
            }
        }
        System.out.println("output : " + nilaiTerbesar+"\n");
        kurangi();
    }

    /**
     * proses mengurangi untuk dicetak
     * membuktikan bahwa nila tidak lebih kecil dari 0 (< 0)
     */
    @Override
    public void kurangi() {
        for (int num : nums) {
            int pengurangan = nilaiTerbesar - num;
            if (num < nilaiTerbesar) {
                System.out.println("hasil dari " + nilaiTerbesar + " - " + num + " = " + pengurangan);
            } else {
                System.out.println("hasil dari " + nilaiTerbesar + " - " + num + " = " + pengurangan + " (nilai terbesar)");
            }
        }

        System.out.println("\n");
        MenuUtama menuUtama = new MenuUtama();
        menuUtama.menu();
    }
}
